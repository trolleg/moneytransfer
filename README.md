# Standalone application money transfer

Description:
- uses standalone jetty
- restful api
- H2 database
- Hibernate

Requirements for package:
- java 1.8+
- maven 3.5.2+
- internet

Just do this:
- mvn clean package

After start:
- java -jar target\account-1.0-SNAPSHOT.jar
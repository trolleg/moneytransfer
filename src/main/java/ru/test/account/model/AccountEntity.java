package ru.test.account.model;

import org.eclipse.persistence.annotations.Index;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "name", columnNames = {"name"}))
public class AccountEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @Index(unique = true)
    private String name;

    private Integer sum;

    public AccountEntity() {}
    public AccountEntity(String name, Integer sum) {
        this.name = name;
        this.sum = sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }
}

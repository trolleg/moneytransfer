package ru.test.account.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class TransferEntity {
    @Id
    @GeneratedValue
    private Integer id;

    private String account1;
    private String account2;
    private int amount;
    private int status = 0;
    private Date date = new Date();

    public TransferEntity() {}
    public TransferEntity(String account1, String account2, int amount) {
        this.account1 = account1;
        this.account2 = account2;
        this.amount = amount;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount1() {
        return account1;
    }

    public void setAccount1(String account1) {
        this.account1 = account1;
    }

    public String getAccount2() {
        return account2;
    }

    public void setAccount2(String account2) {
        this.account2 = account2;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }
}

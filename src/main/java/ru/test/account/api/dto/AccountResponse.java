package ru.test.account.api.dto;

public class AccountResponse {
    private int statusResponse;
    private String account;
    private int sum;

    public AccountResponse() {}
    public AccountResponse(int status, String account, int sum) {
        this.statusResponse = status;
        this.account = account;
        this.sum = sum;
    }

    public int getStatusResponse() {
        return statusResponse;
    }

    public String getAccount() {
        return account;
    }

    public int getSum() {
        return sum;
    }

    public void setStatusResponse(int statusResponse) {
        this.statusResponse = statusResponse;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}

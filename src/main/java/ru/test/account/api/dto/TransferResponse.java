package ru.test.account.api.dto;

public class TransferResponse {
    private int statusResponse;
    private int id;

    public TransferResponse() {}
    public TransferResponse(int status, int id) {
        this.statusResponse = status;
        this.id = id;
    }

    public int getStatusResponse() {
        return statusResponse;
    }

    public void setStatusResponse(int statusResponse) {
        this.statusResponse = statusResponse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

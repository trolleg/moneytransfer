package ru.test.account.dao;

import ru.test.account.api.dto.StatusResponse;
import ru.test.account.model.AccountEntity;
import ru.test.account.db.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;
import java.util.List;

public class AccountDAO {
    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();

    public AccountEntity getAccountValue(String account) {
        EntityManager theManager = emf.createEntityManager();
        List<AccountEntity> entities = theManager.createQuery("SELECT t FROM AccountEntity t where t.name = :value1", AccountEntity.class)
                .setParameter("value1", account).getResultList();
        if (entities.size() == 1) {
            System.out.println("result " + entities.get(0).getSum());
            return entities.get(0);
        }
        theManager.close();
        return null;
    }

    public void updateAccount(AccountEntity accountEntity) {
        EntityManager theManager = emf.createEntityManager();
        theManager.getTransaction().begin();
        theManager.merge(accountEntity);
        theManager.getTransaction().commit();
        theManager.close();
    }

    // update account in one transaction
    public int updateAccountsSum(AccountEntity accountEntity1, AccountEntity accountEntity2, int sum) {
        EntityManager theManager = emf.createEntityManager();
        theManager.getTransaction().begin();
        AccountEntity newAccountEntity1 = theManager.find(AccountEntity.class, accountEntity1.getId(), LockModeType.PESSIMISTIC_WRITE);
        int newSum = newAccountEntity1.getSum() - sum;
        if (newSum < 0) {
            theManager.getTransaction().commit();
            return StatusResponse.ACCOUNT1_NO_MONEY;
        }
        accountEntity1.setSum(newSum);
        theManager.merge(accountEntity1);
        AccountEntity newAccountEntity2 = theManager.find(AccountEntity.class, accountEntity2.getId(), LockModeType.PESSIMISTIC_WRITE);
        accountEntity2.setSum(newAccountEntity2.getSum() + sum);
        theManager.merge(accountEntity2);
        theManager.getTransaction().commit();
        theManager.close();
        return StatusResponse.OK;
    }

    public int createAccount(AccountEntity account) {
        EntityManager theManager = emf.createEntityManager();
        try {
            theManager.getTransaction().begin();
            theManager.persist(account);
            theManager.getTransaction().commit();
        } catch (PersistenceException e) {
            return StatusResponse.ACCOUNT_EXISTS;
        }
        theManager.close();

        return StatusResponse.OK;
    }
}
